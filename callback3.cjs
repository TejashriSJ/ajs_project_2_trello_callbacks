const fs = require("fs");
const path = require("path");

function getCardInfo(listID, cardCallBack) {
  setTimeout(() => {
    if (listID === undefined || cardCallBack === undefined) {
      throw new Error("Parameters passed is Incorrect");
    }
    fs.readFile(path.join(__dirname, "cards.json"), "utf8", (err, data) => {
      if (err) {
        cardCallBack("ERROR: In reading file");
      } else {
        let cardData = JSON.parse(data);
        let cardDetalsOfListID = cardData[listID];
        if (cardDetalsOfListID === undefined) {
          cardCallBack("No details Found");
        } else {
          cardCallBack("", cardDetalsOfListID);
        }
      }
    });
  }, 3 * 1000);
}

module.exports = getCardInfo;

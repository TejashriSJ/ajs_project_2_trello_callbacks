const fs = require("fs");
const path = require("path");
const getBoardInfo = require("./callback1.cjs");
const getListInfo = require("./callback2.cjs");
const getCardInfo = require("./callback3.cjs");

function getAllInfo3() {
  setTimeout(() => {
    fs.readFile(path.join(__dirname, "boards.json"), "utf8", (err, data) => {
      if (err) {
        console.error("Fail to read file", err);
      } else {
        let boardDetails = JSON.parse(data);
        let boardDetalsOfThanous = boardDetails.filter((board) => {
          return board.name === "Thanos";
        });

        let boardIDOfThanous = boardDetalsOfThanous[0].id;

        getBoardInfo(boardIDOfThanous, (err, boardDataOfThanous) => {
          if (err) {
            console.error("Error in getting Thanous data", err);
          } else {
            console.log(boardDataOfThanous);
            getListInfo(boardIDOfThanous, (err, listDataOfThanous) => {
              if (err) {
                console.error("Error in getting Thanous List Data", err);
              } else {
                console.log(listDataOfThanous);

                let cardDetailsArray = [];
                let failedArray = [];
                listDataOfThanous.forEach((listData) => {
                  getCardInfo(listData.id, (err, cardDetails) => {
                    if (err) {
                      failedArray.push(listData.id);
                    } else {
                      cardDetailsArray.push(cardDetails);
                    }

                    if (
                      cardDetailsArray.length + failedArray.length ===
                      listDataOfThanous.length
                    ) {
                      console.log(
                        "Successfully fetched Card details:\n",
                        cardDetailsArray
                      );
                      console.log(
                        "Card details not found for IDs:\n",
                        failedArray
                      );
                    }
                  });
                });
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = getAllInfo3;

const fs = require("fs");
const path = require("path");

function getBoardInfo(boardID, boardInfoCallback) {
  setTimeout(() => {
    if (boardID === undefined || boardInfoCallback === undefined) {
      throw new Error("Argumenet passed is incorrect ");
    } else {
      fs.readFile(path.join(__dirname, "boards.json"), "utf8", (err, data) => {
        if (err) {
          boardInfoCallback("ERROR: In reading file");
        } else {
          let board = JSON.parse(data);
          let infoOfBoardID = board.filter((board) => {
            return board.id === boardID;
          });
          if (infoOfBoardID.length !== 0) {
            boardInfoCallback("", infoOfBoardID);
          } else {
            boardInfoCallback("NOT FOUND");
          }
        }
      });
    }
  }, 2 * 1000);
}

module.exports = getBoardInfo;

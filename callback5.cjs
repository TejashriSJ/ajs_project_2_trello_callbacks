const fs = require("fs");
const path = require("path");
const getBoardInfo = require("./callback1.cjs");
const getListInfo = require("./callback2.cjs");
const getCardInfo = require("./callback3.cjs");

function getAllInfo2() {
  setTimeout(() => {
    fs.readFile(path.join(__dirname, "boards.json"), "utf8", (err, data) => {
      if (err) {
        console.error("Fail to read file", err);
      } else {
        let boardDetails = JSON.parse(data);
        let boardDetalsOfThamous = boardDetails.filter((board) => {
          return board.name === "Thanos";
        });

        let boardIDOfThanous = boardDetalsOfThamous[0].id;

        getBoardInfo(boardIDOfThanous, (err, boardDataOfThanous) => {
          if (err) {
            console.error("Error in getting Thanous data", err);
          } else {
            console.log(boardDataOfThanous);
            getListInfo(boardIDOfThanous, (err, listDataOfThanous) => {
              if (err) {
                console.error("Error in getting Thanous List Data", err);
              } else {
                console.log(listDataOfThanous);
                let MindAndSpaceData = listDataOfThanous.filter((listData) => {
                  return listData.name === "Mind" || listData.name === "Space";
                });
                MindAndSpaceData.forEach((data) => {
                  getCardInfo(data.id, (err, cardDetails) => {
                    if (err) {
                      console.error("Error in getting card data of Space", err);
                    } else {
                      console.log(cardDetails);
                    }
                  });
                });
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = getAllInfo2;

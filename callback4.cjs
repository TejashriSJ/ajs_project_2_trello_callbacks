const fs = require("fs");
const path = require("path");
const getBoardInfo = require("./callback1.cjs");
const getListInfo = require("./callback2.cjs");
const getCardInfo = require("./callback3.cjs");

function getAllInfo1() {
  setTimeout(() => {
    fs.readFile(path.join(__dirname, "boards.json"), "utf8", (err, data) => {
      if (err) {
        console.error("Fail to read file", err);
      } else {
        let boardDetails = JSON.parse(data);
        let boardDetalsOfThanous = boardDetails.filter((board) => {
          return board.name === "Thanos";
        });

        let boardIDOfThanous = boardDetalsOfThanous[0].id;

        getBoardInfo(boardIDOfThanous, (err, boardDataOfThanous) => {
          if (err) {
            console.error("Error in getting Thanous data", err);
          } else {
            console.log(boardDataOfThanous);
            getListInfo(boardIDOfThanous, (err, listDataOfThanous) => {
              if (err) {
                console.error("Error in getting Thanous List Data", err);
              } else {
                console.log(listDataOfThanous);

                let MindData = listDataOfThanous.filter((listData) => {
                  return listData.name === "Mind";
                });
                getCardInfo(MindData[0].id, (err, cardDetailsOfMind) => {
                  if (err) {
                    console.error("Error in getting card data of Mind", err);
                  } else {
                    console.log(cardDetailsOfMind);
                  }
                });
              }
            });
          }
        });
      }
    });
  }, 3 * 1000);
}

module.exports = getAllInfo1;

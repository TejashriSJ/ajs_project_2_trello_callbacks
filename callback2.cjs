const fs = require("fs");
const path = require("path");

function getListInfo(boardID, listCallBack) {
  setTimeout(() => {
    if (boardID === undefined || listCallBack === undefined) {
      throw new Error("Parameter Passed is Incorrect");
    }
    fs.readFile(path.join(__dirname, "lists.json"), "utf8", (err, data) => {
      if (err) {
        boardInfoCallback("ERROR: In reading file");
      } else {
        let listsData = JSON.parse(data);
        let infoOfListID = listsData[boardID];
        if (infoOfListID === undefined) {
          listCallBack("No Information related to given  ID");
        } else {
          listCallBack("", infoOfListID);
        }
      }
    });
  }, 2 * 1000);
}

module.exports = getListInfo;
